/*********************************
* Class: MAGSHIMIM C2			 *
* Week:                			 *
* Name:                          *
* Credits:                       *
**********************************/

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

#define FALSE -1
#define TRUE !FALSE
#define BINARY_READ "rb"
#define WRITE_PLUS "w+"
#define FIRST_TWENTY 0.2
#define LAST_TWENTY 0.8
#define WRITE_FILE "AntiVirusLog.txt"

int welcomeMenu(FILE * writeFile, char folder[], char virus[]);
int calcFileLen(FILE * ptr);
int fullFileScan(FILE * file, FILE * virus, int virusLen);
int quickFileScan(FILE * file, FILE * virus, int virusLen, int fileLen);


int main(int argc, char *argv[])
{
	if (argc < 3)
	{
		printf("\nInsufficient Arguments: \n");
		printf("\nHelp:./executable <folder to scan> <virus signature>\n");
		exit(1);
	}

	struct dirent *dir;
	DIR * dr = opendir(argv[1]);
	FILE * virus = fopen(argv[2], BINARY_READ);
	FILE * file;
	FILE *writeFile;
	
	int virusLen = 0;
	virusLen = calcFileLen(virus);
	int choice = 0;
	int cnt = 0;
	int fileLen = 0;
	char *filePath = malloc(256);
	char *writePath = malloc(256);
	int status = 0;

	strcpy(writePath, argv[1]);
	strcat(writePath, WRITE_FILE);

	writeFile = fopen(writePath, WRITE_PLUS);
	
	choice = welcomeMenu(writeFile, argv[1], argv[2]);

	if (dr)
	{
		printf("Scanning began...\nThis process may take serveral minutes...\n\nScanning:\n");
		fprintf(writeFile, "Scanning began...\nThis process may take serveral minutes...\n\nScanning:\n");
		while ((dir = readdir(dr)) != NULL)
		{
			strcpy(filePath, argv[1]);
			strcat(filePath, dir->d_name);
			file = fopen(filePath, BINARY_READ);

			if (file) 
			{
				fileLen = calcFileLen(file);
				if (choice == 0)
				{
					if (fullFileScan(file, virus, virusLen) == TRUE)
					{
						printf("%s - Infected!\n", filePath);
						fprintf(writeFile, "%s - Infected!\n", filePath);
					}

					else
					{
						printf("%s - Clean\n", filePath);
						fprintf(writeFile, "%s - Clean\n", filePath);
					}
				}
				else
				{
					status = quickFileScan(file, virus, virusLen, fileLen);
					if (status == 1)
					{
						printf("%s - Clean\n", filePath);
						fprintf(writeFile, "%s - Clean\n", filePath);
					}
					else if (status == 2)
					{
						printf("%s - Infected! (first 20%)\n", filePath);
						fprintf(writeFile, "%s - Infected! (first 20%)\n", filePath);
					}
					else
					{
						printf("%s - Infected! (last 20%)\n", filePath);
						fprintf(writeFile, "%s - Infected! (last 20%)\n", filePath);
					}
				}
			}
		}
		closedir(dr);
	}
	else
	{
		printf("Error! No files in the folder!");
		fprintf(writeFile, "Error! No files in the folder!");
	}
	
	printf("Scan Completed.\nSee log path for results: %s", writePath);

	fclose(virus);
	fclose(writeFile);

	getchar();
    return 0;
}

int calcFileLen(FILE * file)
/*
	This function returns the length of a binary file by counting char by char.
	input: the file
	output: the length of the file
*/
{
	int len = 0;
	unsigned int ch;
	fseek(file, 0, SEEK_SET);
	while ((ch = fgetc(file)) != EOF)
	{
		len++;
	}
	return (len);
}

int quickFileScan(FILE * file, FILE * virus, int virusLen, int fileLen)
/*
	This function searches for the virus signature in the input file by comparing both of them char by char from the first 20% of the file and from the last 20% of the file. if the virus was found in the first 20% it returns 2, if it was found in the last 20% it returns 3 and if it wasn't found at all it returns 1.
	input: the input file, the virus signature file, the lenght of the virus file and the length of the input file
	output: not found - 1, found in the first 20% - 2, found in the last 20% - 3
*/
{
	int ch1 = 0;
	int ch2 = 0;
	int cnt = 0;
	int flag = 0;
	int i = 0;
	int status = 0;
	status = 1;
	flag = FALSE;
	fseek(virus, 0, SEEK_SET);
	fseek(file, 0, SEEK_SET);
	while ((ch1 = fgetc(file)) != EOF && (ch2 = fgetc(virus)) != EOF && cnt != virusLen && i != (int)(FIRST_TWENTY * fileLen))
	{
		if (ch1 == ch2)
		{
			cnt++;
			flag = TRUE;
		}
		else
		{
			cnt = 0;
			fseek(virus, 0, SEEK_SET);
			flag = FALSE;
		}
		i++;
	}

	if (flag == FALSE)
	{
		ch1 = 0;
		ch2 = 0;
		fseek(virus, 0, SEEK_SET);
		fseek(file, (int)(LAST_TWENTY * fileLen), SEEK_SET);
		i = 0;
		while ((ch1 = fgetc(file)) != EOF && (ch2 = fgetc(virus)) != EOF && cnt != virusLen)
		{
			if (ch1 == ch2)
			{
				cnt++;
				flag = TRUE;
			}
			else
			{
				cnt = 0;
				fseek(virus, 0, SEEK_SET);
				flag = FALSE;
			}
			i++;
		}
		if (flag == TRUE)
		{
			status = 3;
		}
	}
	else
	{
		status = 2;
	}

	return (status);
}

int fullFileScan(FILE * file, FILE * virus, int virusLen)
/*
	This function searches for the virus signature in the input file by comparing them char by char. if the virus was found, it returns TRUE and if it wasn't it returns FALSE.
	input: the input file, the virus signature file and the virus length
	output: the virus was found in the file - TRUE, the virus wasn't found in the file - FALSE
*/
{
	int ch1 = 0;
	int ch2 = 0;
	int cnt = 0;
	int flag = 0;
	flag = FALSE;
	fseek(virus, 0, SEEK_SET);
	fseek(file, 0, SEEK_SET);
	while ((ch1 = fgetc(file)) != EOF && (ch2 = fgetc(virus)) != EOF && cnt != virusLen)
	{
		if (ch1 == ch2)
		{
			cnt++;
			flag = TRUE;
		}
		else
		{
			cnt = 0;
			fseek(virus, 0, SEEK_SET);
			flag = FALSE;
		}
	}

	return (flag);
}

int welcomeMenu(FILE * writeFile, char folder[], char virus[])
/*
	This function is the welcome function which prints the welcome messages and scans for a choice from the user if he wants to use the quick scan or the full scan. Also, the function writes the welcome message and the user's choice to the writing log file.
	input: the writing log file, the path of the folder, the path of the virus signature file
	output: the user's choice (0 for full scan and any other number for quick scan)
*/
{
	int choice = 0;
	printf("Welcome to my Virus Scan!\n\nFolder to scan: %s\nVirus signature: %s\n\nPress 0 for a normal scan or any other key for a quick scan: ", folder, virus);
	scanf("%d", &choice);
	getchar();

	fprintf(writeFile, "Welcome to my Virus Scan!\n\nFolder to scan: %s\nVirus signature: %s\n\nPress 0 for a normal scan or any other key for a quick scan: %d\n", folder, virus, choice);

	return (choice);
}
